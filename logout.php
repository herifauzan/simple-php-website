<?php
require_once "controller/Base.php";
session_start();
// remove all session variables
session_unset(); 

// destroy the session 
session_destroy(); 
$base = new Base();
$base->redirect("./login.php");

?>
