<?php
require_once "model/Template.php";
require_once "model/User.php";
require_once "model/Model.php";

class Base {
	function __construct(){
		$view = new Template();
		$this->view = $view;
	}

	function render_header($title,$highlight){
		$user = $this->user->data;
		$view = new Template();

		$view->header_catalog_class = "";
		$view->header_your_product_class = "";
		$view->header_add_product_class = "";
		$view->header_sales_class = "";
		$view->header_purchases_class = "";

		$view->header_catalog_link = "./home.php";
		$view->header_your_product_link = "./shop.php?action=browse";
		$view->header_add_product_link = "./shop.php?action=add_product";
		$view->header_sales_link = "./shop.php?action=sales";
		$view->header_purchases_link = "./purchases.php";
		$view->header_logout_link = "./logout.php";

		$view->user = $user;
		$view->title = $title;

		$view->__set("header_".$highlight."_class","header-highlight");

		return $view->render_return("header.php");
	}

	function init_db(){
		if( isset($this->db) )
			return;

		$mysqli = new mysqli(DB_HOST, DB_USER, DB_PASS, DB_NAME);
		if( $mysqli->connect_errno ){
			throw new Exception("Cannot connect to database");
		}
		$this->db = $mysqli;
	}

	function init_user(){
		session_start();
		$user_id = $_SESSION["user_id"];//$_GET['user_id'];
		if( empty($user_id) )
			return $this->redirect("./");

		$user_id = $_SESSION['user_id'];
		$this->init_db();

		$this->user = User::with_id($user_id);
	}

	function redirect($url){
		header("Location: $url");
	}
}
?>
